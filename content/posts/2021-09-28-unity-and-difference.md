---
title: Wanting unity, but being different
date: 2021-09-25
---

I have noticed a fundamental tension in my own thoughts, and in the thoughts of others. Here is the problem: I recognize the interdependence of people, and the limitations of extreme individualism, and want to encourage greater collective action. But in practice, I think that collective action in our society would lead to worse outcomes. For example - I think if our society was more collective, we would have less questioning of governmental and media narratives around foreign wars. I think this would be bad! I am glad that we can condemn their actions. 

On a personal level, I am glad that I was able to do things like:
- move away from my home community for school and work without fighting community pressure
- hold different moral and religious beliefs than the community around me
- get a job that didn't exist 50 years ago, and that many people don't think should exist today
- and in many ways, live a life completely different than other people

Is it possible for me, and for others like me, to reconcile a desire for greater unity with holding idiosyncratic beliefs? Here are some possible syntheses.

## Convergence of beliefs
If there is a mechanism for people to collectively discuss and update their ideas, perhaps eventually these ideas will lead towards truth, and my unique ideas, if true, will become widely accepted. Unfortunately, I don't think this is true. Previous social advancements, like overcoming prejudice or oppression, seemed to require people who lived a life that demonstrated new possibilities, in the face of widespread condemnation. Would we have advanced past 1950s gender roles without the example of women who successfully broke those roles?

## Tradeoff of values vs. effectiveness
A unified society can accomplish much more, and even though we may not share all of the values that our society does, the greater effectiveness in implementing the values that we DO share outweighs the losses. This only holds if we think that society's values or ideas are mostly aligned with our own. Unfortunately, I can't agree with this statement either - although some people could.

## A unified society - and I am in charge (or, someone like me is in charge)
See: dictatorship. Also see: this will never happen

## Let a thousand flowers bloom
A unified society recognizes the need for experimentation, and provides spaces for it, while ensuring that they don't overstep the bounds of acceptibility, and the best ideas can become widely adopted. I think this runs the risk of getting trapped in local minima - I don't trust that society will be able to identify how much experimentation is really needed. A common theme in the startup world is that startups seem really stupid, harmful, or useless - and then they are billion dollar industries that have reshaped the world (ex. Google, Uber, Airbnb, Facebook). Would a more collectivist society have allowed these startups to exist? Maybe it wouldn't, and that's a good thing.


## Improvement of society
A more collective society right now, with current ideas and current people, would be bad. But in the future, with better morals and better ideas, it would work well. So we should work towards the betterment of society, and also towards greater unity. This is possible, but I'm just not that optimistic.


## Conclusion
The common theme in my hesitation to accept these answers is that I lack trust in the institutions of society and the community. Right now, I square this circle by working with a community and institutions that I do trust, engaging with wider society, and hope that will be enough. 
