---
title: Collective intuitive reasoning
date: 2020-02-27
---

There are two reasons people typically give to justify using intution over careful reasoning:
1. Intution allows you to draw on your experience and make decisions quickly and correctly.
2. Intution lets you reach an answer that you wouldn't discover using logical reasoning. If you ignore the intuition, you'll get the wrong answer.

In practice, I don't think the choice is between logical reasoning and intuition - it's between using "accepted" justifications and "forbidden" justifications, which depends on the social context. In almost all cases, reasoning by comparison to past experience is "accepted". In religious circles, making a decision based on reading holy writings is "accepted" - in others, it's not. Part of the difference depends on what authorities are trusted - the news, scientists, a particular politician, a mutual friend. But I think this is not the **only** difference - the *type* of reasoning used also varies. For example, in some circumstances, you might be able to justify an action by saying that its expected value is positive - but in another situation, this reasoning won't get you anywhere. It's not just a different level of risk tolerance - it's a norm of decision-making.

One norm I think is important for a group to have is to be able to explicitly use "forbidden" justifications. If you don't, then unless you've chosen exactly the right set of acceptable justifications, then you will consistently make mistakes. If you are able to make a claim while simultaneously stating that you can't (under the current decision-making norms) justify it, then it's possible to test out possible justifications with less risk.
