---
title: Is Eco-tourism worth it?
date: 2019-11-05
---

(Guest post from Nabil Kalantar)

Let’s analyze one case of eco-tourism, and make some unsubstantiated assumptions, just to get a sense of the problem.


## Absorbed
We will look at the rainforests in Costa Rica, a country which receives 1 million visitors from the U.S. each year, and 700,000 not from the U.S.

**Assumption**: All these tourists are there to visit the rainforest.

28% of the land in Costa Rica is protected.

**Assumption**: The sole purpose of this protection is tourism, and it would not be protected if it weren’t for tourism.

**Assumption**: All of the protected land is rainforest.

We want to know how much CO2 is absorbed per square mile. I couldn’t find this number anywhere, but I did find information on that would allow me to calculate it. The Amazon, which has an area of 2.124 million square miles, absorbs 2 billion tons of CO2 per year, so \~1000 tons of CO2 are absorbed per square mile of rainforest.

**Assumption**: The CO2 consumption of Costa Rican rainforest is comparable to that of the Amazon.
The area of Costa Rica is 19,730 square miles, with 28% protected, giving us 5,524 square miles of rainforest, which would absorb 11 billion tons of CO2 annually.

## Emitted

**Assumption**: All 1 million U.S. visitors to Costa Rica fly there from Houston (the closest major airport, at a distance of 1500 miles.

A plane releases \~53 lbs of CO2 per mile.

**Assumption**: There are 100 people on each flight

So a single traveler to Costa Rica releases 795 lbs of carbon dioxide in their flight, for a total of 795 million tons of CO2 per year.

This is less than the amount absorbed annually — but most of our assumptions favored higher absorption and lower emission (ignoring 700,000 tourists, only flying from Houston…). So the final conclusion is that it’s a close-ish call — at least worth looking into a little more.

**Final note from Na’im and Varqa**: Potentially, this analysis leaves out the dominant factor, which is replacement effects. If trips to Costa Rica are replacing trips to Europe, then the amount of CO2 saved by the closer trip might outweigh any effect of absorption by the rainforest itself. Either way, it’s interesting to see that despite the bad press, eco-tourism might actually be environmentally friendly (from a certain point of view).

**True final note**: Na’im would like to suggest that instead of encouraging tourism to Costa Rica, we instead dump iron in the water to bolster fish and whale populations. Apparently whales are the new trees — in terms of absorbing carbon dioxide, and also in terms of being too big and majestic to comprehend.

![whale](/img/whale.jpg)
Amazingly, the first whale image on Google is from an article about whales absorbing CO2… and doing an economic-style analysis on how much whales are worth. I think Google is getting too smart. (From [Treehugger](https://www.treehugger.com/animals/startling-reason-whales-could-be-worth-2-million-each.html))