---
title: Assorted Wedding Thoughts
date: 2020-01-31
---

### Tips for planning a simple, inexpensive wedding

- The hardest, but most important advice: keep your preferences [rational](https://en.wikipedia.org/wiki/Rational_choice_theory). It is easy to think nothing of spending an additional $500 on a $5000 wedding, since it's only a 10% increase. DO NOT BE FOOLED. Apply the same level of scrutiny to that $500 expenditure as you would in your daily life.
- Example of the above: My wife bought a white dress for $20. I wore the same suit I wore to my high school graduation. Think carefully about how important a wedding dress/suit is to you.
- You should have a venue, food, a ceremony program, and (potentially) dancing. Everything else is optional. It's better not to do something than to do it half-heartedly.
- Choose one or two things to focus on yourselves. Delegate everything else, and disconnect yourself from the details. 
- The venue will probably be expensive. If you choose a venue that requires you to use their caterers, it will be incredibly expensive. To avoid this issue, you will probably have to avoid typical wedding venues.
- You probably have friends that can do a passable job for free or cheap for photography, decoration, and making a playlist. I would not recommend this approach for food - cooking good food for a lot of people is really difficult.
- If you're planning an inexpensive wedding, you should encourage guests not to stress about gifts. I don't have a good way to do this.
- I highly recommend banning photographs or videos during the ceremony. It adds power.
- Ignore all advice except mine. Just kidding.

***
### Why is wedding planning so feminine? 
For heterosexual weddings, 50% of the participants are male. So why are weddings so feminine?

Some possible explanations:
- Weddings are big events, and event planning is generally associated with women.
- Weddings are the beginning of a family, and families are associated with women.
- The wedding planning industry has targeted women, and encouraged them to have big expectations.

These don't seem very convincing to me. The first seems shaky - is event planning REALLY so heavily associated with women? The third doesn't really add any explanatory power. The second implies that weddings should be female-dominated in all cultures and in the past, which seems like a reasonable test. Unfortunately, I have been unable to find any source that addresses this question. Most sources take it for granted that weddings were planned by women, regardless of culture or time period.

Various sources that I have found suggest that big weddings in the West are a result of large royal weddings ([Queen Victoria](https://scholarsarchive.jwu.edu/cgi/viewcontent.cgi?article=1037&context=student_scholarship), or more recently, [Lady Diana](https://weddingsforaliving.com/the-history-of-the-wedding-planner)).

My current guess is that, by chance, or for specific historical reasons, these weddings appealed more to the female population of the West. Wedding culture and the wedding industry sprung up in response to the new demand for large weddings, and further encouraged the existing trend. This leads us to the present day, where a small part of my brain was screaming at me while writing this post, telling me that femininity is just an inherent feature of weddings, just like how water is inherently wet, and there's nothing to explain.

***
### An alternate reality
Let us imagine a world in which weddings were considered masculine. Is it easy to picture?
Perhaps the focus would be a large feast, followed by drinking and partying with all of the groom's friends (and the bride's as well). The ceremony could emphasize themes of responsibility, persistence, and dedication (male-coded traits). 

Since these are also true of the current world, why should we not consider weddings to be celebrations of masculinity? (Note: I actually came up with most of this list by trying to imagine a world different from ours, and I couldn't do it.)

