---
title: Three GDP Questions
date: 2020-01-02
---

### Why didn't GDP double as women entered the workforce?
![GDP graph](/img/US-GDP.png)

Answer: It did, according to [Janet Yellen](https://www.federalreserve.gov/newsevents/speech/yellen20170505a.htm), former Federal Reserve chair. Half a percentage point (out of 2–5 percentage points) of annual GDP growth for the last several decades was due to women entering the workforce. Surprisingly, a random speech was the only place I could find that mentioned this number, and the source within the speech was from calculations done by the Federal Reserve using BEA data. Weird, but I think I might just not be looking in the right places. I did find lots of recommendations that developing countries could increase GDP by increasing female participation in the workforce… and none of them cited any sources.

***

### How bad is it that GDP doesn't include housework and childcare?

Pretty much everyone is in agreement that in an ideal world, housework and childcare would be included in GDP. The only reason they’re not is because of the difficulty of measuring the value.

However, some people have tried to measure it anyway, and have found that the total value of housework and childcare in developed countries is likely 30–60% of GDP — a HUGE adjustment. And it might be even higher in developing countries.

And what’s even worse, one study found that the value of childcare/housework a fraction of GDP changed on the order of 10 percentage points over 20. AND there was significant difference in value of childcare/housework between Germany, Canada, and the US (the three countries with the most data). So this really seems like a big deal. From the earlier question, how much of the increase in GDP from women entering the workplace was due only to increasing legibility of already existing production?

Sources:
http://gpiatlantic.org/pdf/housework/housework.pdf 
http://www.oecd.org/economy/outlook/34252981.pdf

***

### What does the future hold?
Look at the most recent few years on the graph we saw earlier:
![GDP graph](/img/US-GDP.png)

The future looks different than the past, and not in a good way.