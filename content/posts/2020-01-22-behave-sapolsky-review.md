---
title: A review of "Behave" by Robert Sapolsky
date: 2020-01-22
---

![Cover of Behave](/img/behave-sapolsky.jpg) 

I learned two things from this book - one from the first half, and one from the second. The first, Robert Sapolsky wanted me to learn. The second - I don't think so.

But first - a quick introduction. *Behave* describes "the biology of humans at our best and worst". More specifically, it investigates the biology of aggression and altruism, from neurons to psychology to culture. Many of the books chapters are laid out like circles surrounding an action - for example, reaching out to touch someone's shoulder, or pulling a trigger. Sapolsky reminds us that the motivation for the same action can be good or evil - a hand reaching out to offer compassion, or a hand reaching out to shove someone. A finger on the trigger in a genocide, or pulling the trigger to defend an innocent bystander. 

We start by looking at the seconds before the act - the realm of the brain. We dive into neurons and action potentials, and understand how a decision in the brain results in a physical movement. We zoom out to a few minutes, and see various parts of the brain, and how they were involved (learning along the way how scientists discovered these connections). We zoom out further - hours or days before, and think about the effects of hormones - oxytocin, adrenaline, testosterone. Zoom out further - the effects of childhood and parenting, and of our genes. Further - the effects of culture. Further - the evolution of human beings.

If this sounds interesting, then I recommend this book. Fair warning - although the book presumes little knowledge of biology (there are appendices with introductions to neuroscience, endocrinology, and proteins), once a concept is introduced, Sapolsky expects you to remember it. I have to admit, I have no idea what region of the brain the vmPFC is, or what significance it has if it's activated. But it seems to be important because it activates at least once every couple pages in this book.

And that leads us nicely into the first lesson:

***

### Everything is connected

A particular neurotransmitter is released, and it results in lowering the activation threshold for some associated neurons. Except - in the presence of a different neurotransmitter, it has the opposite effect. Except - the receptors on the neuron can affect how the neurotransmitter is interpreted.

And then it gets even worse, because that same neurotransmitter also serves as a step in generating a hormone. So the short term effect might be to increase brain activation in a region, but in the long term, the hormone decreases activity in that region.

And everything is like this, and they're all related to each other, up and down the levels, around and around in circles. Childhood trauma affects the genes through epigenetics, and the neurons through stress hormones, and the connections between brain regions since different regions become more important... and then each of these affects our behavior, which then affects our brain, genes, body, hormones...

To recap - any particular external influence (external to the system at a particular level - it might be a cell, an organ, a body, a person) has effects that depend on context - like our neurotransmitter in the first example. And that external influence will have short- and long- term effects that change how the next influence will be interpreted.

But saying that "everything is connected" is equally informative as saying "nothing is connected". The true value lies in discovering which connections are important and causal, and which are not. And this book does a great job of laying out some important connections, and how they were discovered. But this leads to the second thing I learned...

### Biology is too hard

Even in the chapters focusing only on interactions among brain regions, I recognized that Sapolsky was sometimes overstating his case (although in general, he is very careful to specify how confident we should be in any given statement). But then we get to the later chapters, where we take the biological context that we built up in the first half and apply it to the big questions - what makes us moral? why and how do we form hierarchies? what allows aggression and altruism on a societal scale? - that the science falls apart. 

Most of the interesting results that Sapolsky cites - that certain cultures have tendencies towards cooperation or competition, that unclean environments make us more cautious towards outsiders, that a particular action can increase our altruism - he depends on priming studies. In the typical case, a word or image is quickly shown to the subject - for example, either the word "doctor". Then that person will recognize the word "nurse" faster than an unrelated word. An interesting effect - and one that has been almost entirely a [casualty of the replication crisis](https://replicationindex.com/2017/02/02/reconstruction-of-a-train-wreck-how-priming-research-went-of-the-rails/). 

This doesn't mean the conclusions Sapolsky draws are wrong - it could be that priming studies are just easy to write about, or are the most accessible to the non-biologist audience. But it doesn't give me much confidence that anything in the last half of the book is evidence of... anything. When I read it, I tried to remind myself of that fact every page. I even wondered if I should stop reading, so as not to contaminate myself by thinking I'm receiving evidence when I'm really not. But in the end I perservered. If I were you - I wouldn't. The last half of the book is fascinating and gives you a new perspective on many important questions. And that's why it's dangerous - because that new perspective is founded on faulty studies, giving us no reason to privilege it over the boring old perspective we already have. You should only read it so that you can identify the things you already believe that are mentioned in this book, and then decrease your confidence in those beliefs.

I joke - it's not that bad. But it makes me realize how difficult this all is. In his writing, Sapolsky comes across as thoughtful, meticulous, not afraid of controversy, but also not drawn to shocking statements only for the sake of contrarianism. He appreciates nuance, and is incredibly intelligent. And still - he was unable to find the truth. 

So be careful when it comes to biology and especially social science - hesitate to throw out your own observations for the sake of science. 