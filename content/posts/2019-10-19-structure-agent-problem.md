---
title: The structure-agent problem
date: 2019-10-19
---

>"We cannot segregate the human heart from the environment outside us and say that once one of these is reformed everything will be improved. Man is organic with the world. His inner life moulds the environment and is itself also deeply affected by it. The one acts upon the other and every abiding change in the life of man is the result of these mutual reactions."
>
>(Letter written on behalf of Shoghi Effendi, To an individual believer, 17 February 1933)

![A mother and child walking through a destroyed city](/img/destroyed-city.jpg)

When you are ten years old, Mohamed Bouazizi lights himself on fire, and several weeks later, the Syrian Civil War begins.

By the time you are twelve, you are accustomed to seeing videos of fighting in Syria, and hearing about the horrors of war.

When you reach fifteen, you have met many Syrian refugees, and become friends with them, and heard the stories of their lives. You have met people who don’t know if their brothers are alive, and you’ve met people that know for sure their brothers are not.

At the age of eighteen, you’ve recognized that this is the problem you want to solve in your life, so you major in international relations with a minor in political science. You graduate with a 4.0 GPA, get accepted to a prestigious law school, and take a job as a congressional aide. Within ten years, you have your own congressional aides. Within thirty, you are the President of the United States of America.

The civil war in Syria is over, and your own children don’t even know it ever happened. But other conflicts and wars are ongoing, new ones cropping up all the time.

You gather your advisors together and tell them — establishing world peace is our priority.

What do they say?

![JFK at the fall of the Berlin wall](/img/jfk-berlin-wall.jpg)

Do they say "right away, boss", and every problem is quickly solved?

Do they get right to work, and four years later you’re leaving office with the world on fire and no one sure how it got to that point?

Or maybe the director of the CIA sits you down, walks you through the geopolitical situations, patiently explains why you can’t do this and you can’t do that — or that the conflict in this region is inevitable because of the lack of resources, and that war surely won’t die down because the ethnic tensions are too long-lasting.

When you’re the decision-maker for the most powerful nation in the world, do you have any power at all?

This is the structure-agent problem.

***

To lower the stakes slightly, consider Obama. He promised to close Guantanamo Bay. People supported his closing of Guantanamo Bay. He tried to close Guantanamo Bay. And a decade after his election, the Wikipedia page still starts with "Guantanamo Bay **is…**"

***

To lower the stakes even further, imagine that you own a small manufacturing business. If you decide that you’re going to run an ethical company that treats its workers and customers well, and avoid the problems that other companies cause — what will happen to you? If you have higher prices to pay your workers more, will consumers leave you for another company? Do they have that choice? What constrains your actions?
Or imagine that you run ExxonMobil. If you’re totally dedicated to combating global warming, can you accomplish any meaningful goals? Or will the mass of consumers hungry for energy simply turn to your competitors if you show any sign of standing between them and their cheap air conditioning?
This is the structure-agent problem.

***

So you can obviously say that both matter — the individuals, and the system that they’re a part of. Equally obviously, you can ask, "but how *much* do they matter?""

I would love to know how people study this question, but here are some initial thoughts:

- If you’re operating in a highly competitive realm with a long history, the structure probably matters more.
- If you’re in a poorly-understood field, or a new opportunity, an individual agent could make decisions with long-lasting impacts
- If people in a different place or time have established a different system, then determined agents could conceivably shift the whole structure to a different one — we already know it’s possible!
- If, conversely, the same structure reappears time and again, you might not be able to change it regardless of what power you hold.
- For a small group of like-minded individuals, I suspect that you can set up structures that are radically different than the society around you. But if you try to scale them up, they will fail.


What I really want is a framework that will tell me that oil+gas companies have 21.3% freedom, and consumer demand structure sets 43% of the structure, and government regulations affect the rest. I guess this is impossible. But at least I wish I had something better than making up heuristics.