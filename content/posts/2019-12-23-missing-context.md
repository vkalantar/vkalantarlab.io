---
title: Missing context
date: 2019-12-23
---

![A chameleon](/img/chameleon.jpg)

(NOTE: After I finished writing this, I realized that most of these ideas are found in the last section of the junior youth text Spirit of Faith. I’m publishing it anyway because I found it helpful even though I’ve studied that book.)

Here is a short snippet from Some Answered Questions, #62:

> …[P]arents endure the greatest toil and trouble for their children, and often, by the time the latter have reached the age of maturity, the former have hastened to the world beyond. Rarely do the mother and father enjoy in this world the rewards of all the pain and trouble they have endured for their children. The children must therefore, in return for this pain and trouble, make charitable contributions and perform good works in their name, and implore pardon and forgiveness for their souls. You should therefore, in return for the love and kindness of your father, give to the poor in his name and, with the utmost lowliness and fervour, pray for God’s pardon and forgiveness and seek His infinite mercy.

This seems relatively straightforward — our parents sacrifice for us on this earth, and since we may not be able to repay their efforts directly, we should give to the poor in the name of our… fathers? But not our mothers? And if this is a duty that we all have, why does it seem like no one ever mentions it?

For once, these questions have an answer — contained in footnote 147:

> ‘Abdu’l-Bahá is here directly addressing Laura Clifford Barney, whose father had passed away in 1902.

The right context takes a confusing quote, and makes it intelligible.

How many other quotes did I misunderstand, because I didn’t know the context?

***

The previous example was straightforward, since the new translation of Some Answered Questions contains this footnote. And to be honest, I had never encountered this quote or thought about it, until after I already knew the context. But here’s a case that has confused me since I read it — one that has stuck with me for ten years, and that I’ve discussed with multiple people, and is important enough that it is referenced in the titles of two junior youth texts : [question 36, the five kinds of spirit](https://www.bahai.org/library/authoritative-texts/abdul-baha/some-answered-questions/8#253698687).

Since when are there five kinds of spirit? I thought only humans had souls? And now vegetables do? And there’s the “spirit” and the “rational soul” and is that different than the regular “soul” that continues on after we die?

According to Na’im, the right context for this question is Aristotle, so let’s re-examine this passage with that in mind.

In a [separate question (Q55)](https://www.bahai.org/library/authoritative-texts/abdul-baha/some-answered-questions/10#819518206), ‘Abdu’l-Bahá explains:

>…in general, spirit is divided into five categories: the vegetable spirit, the animal spirit, the human spirit, the spirit of faith, and the Holy Spirit.

(A footnote also mentions that He includes the mineral spirit in other talks)

In this question, He discusses each in more depth:

The **vegetable spirit** appears when a certain amount of physical elements is combined into a particular structure, and its appearance brings with it an “ associated power of growth”. When the structure is destroyed, so too is the spirit.

The **animal spirit** is similar — a combination of elements in a structure that brings with it the power to “ perceives sensible realities — that which can be seen, heard, tasted, smelled, or touched”. Again, when the elements are separated, the spirit ceases to exist.

The **human spirit** is different — when the elements are separated, the spirit continues to exist. This sounds a lot like “the soul”, and indeed ‘Abdu’l-Bahá makes this connection explicit in question 55. The associated power is “ the discovering power that encompasses all things. All the wondrous signs, all the crafts and discoveries, all the mighty undertakings and momentous historical events of which you are aware, have been discovered by this spirit and brought forth from the invisible realm into the visible plane through its spiritual power.”

What I didn’t know until now, is that these descriptions of the vegetable, animal, and human spirit line up pretty much exactly with Aristotle’s classification in [*On the Soul*](https://en.wikipedia.org/wiki/On_the_Soul) — including the persistence of the human spirit after death. The terminology is slightly different, but after reading both, it is clear that they refer to the same context.

So this answers our first question — there are five kinds of spirit, if by “spirit” you mean what Aristotle meant. This doesn’t mean that ‘Abdu’l-Bahá is confirming that this is a singularly useful way of looking at the world (more on this later. Let’s look at the next two degrees of spirit.)

***

Now we diverge from Aristotle — if any descriptions of the next two kinds of spirit existed before ‘Abdu’l-Bahá, I don’t know about it (please tell me if they do).

The spirit of faith, the description of which I quote in full:

>As to the fourth degree of spirit, it is the heavenly spirit, which is the spirit of faith and the outpouring grace of the All-Merciful. This spirit proceeds from the breath of the Holy Spirit, and through a power born of God it becomes the cause of everlasting life. It is that power which makes the earthly soul heavenly and the imperfect man perfect. It cleanses the impure, unlooses the tongue of the silent, sanctifies the bondslaves of passion and desire, and confers knowledge upon the ignorant.

And the Holy Spirit: (not the full description)

>The fifth degree of spirit is the Holy Spirit, which is the mediator between God and His creation. It is like a mirror facing the sun: Just as a spotless mirror receives the rays of the sun and reflects its bounty to others, so too is the Holy Spirit the mediator of the light of holiness, which it conveys from the Sun of Truth to sanctified souls. This Spirit is adorned with all the divine perfections. Whensoever it appears, the world is revived, a new cycle is ushered in, and the body of humanity is clothed in a fresh attire. It is like the spring: When it arrives, it transports the world from one condition to another. For at the advent of springtide the black earth, the fields, and the meadows become green and verdant; flowers and sweet-scented herbs of every kind spring forth; trees are endowed with a new life; wondrous fruits are produced; and a new cycle is inaugurated.

The Holy Spirit has generally made sense to me, even before understanding the context. What powers exist beyond our human comprehension, the same way we possess powers beyond that of the animal? The power of the Holy Spirit, that of the Manifestations of God. I interpret this to mean that in the same way that the vegetable spirit is associated with vegetables, the animal with animals, the human with humans, the Holy Spirit is associated with the Manifestations of God.

And by casting this explanation into the language of Aristotle, ‘Abdu’l-Bahá helps us to understand the station of the Manifestation:

1. They possess powers beyond merely the extreme range of human abilities — there is a fundamental difference between us and Them.
2. They possess the power of the rational mind, just as we, although we are not animals, still can use the power of the senses.
3. That Holy Spirit, like our own rational soul, continues to exert its influence after the physical death of the Manifestation.

Again, this does not necessarily mean that Aristotle had the right divisions of spirit, 3000 years ago, and he just didn’t know about two of them. It just means that, if we were to adopt this particular framework, it would help us understand better the station of the Manifestation.

***

I skipped the spirit of faith — because I find it confusing. Each of the other degrees of spirit shared certain properties — they were associated with a power, and with a physical being that manifested that power, along with the powers of the previous levels. So what is the physical being associated with the spirit of faith. According to one of my friends, the answer is “religion”. This reading seems to match the text — especially that the spirit of faith “proceeds from the breath of the Holy Spirit”. I was hoping that looking at it with new context would give me new insight by the time I finished writing this post… but it didn’t. Oh well. I’ll have to wait a few more years and try again.