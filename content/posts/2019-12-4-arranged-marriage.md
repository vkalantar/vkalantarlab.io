---
title: How to get married
date: 2019-12-04
---


## Reasons to get an arranged marriage

1. You will for sure have the consent of the parents.
2. When someone else makes a decision for you, they are further removed from the situation, which helps avoid some biases in thinking.
3. If your parents have a good relationship, then you know they have a proven ability to choose good spouses. You have no such experience.
4. If things go bad, you have someone else to blame
5. You can tell your kids that arranged marriages are great, and you don’t have to worry about them marrying someone you don’t like.
6. If you don’t have to think about marriage, then you can free up a large part of your brain to make other decisions, like what color socks to wear to your wedding. Just kidding — black socks if you’re male, no socks if you’re female. Next question.

## Reasons not to get an arranged marriage

1. You will take more responsibility if you make your own decisions.
2. You are secretly leading a double life, so your family doesn’t actually know who would make a good spouse for you.
3. You have already met someone you would like to marry, and they are not the person your parents have arranged.
4. You are contrary by nature and will dismiss suggestions just because other people made them.

Since there are 6 reasons in favor, and 4 against, and 6 is bigger than 4, clearly arranged marriage is the way to go.