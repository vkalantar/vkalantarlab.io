---
title: Reputation as a Lossy Compression Algorithm
date: 2021-09-30
---

*Warning: this is probably restating obvious ideas that everyone already knows*

One view of reputation is that it's like a line of credit. Take the example of a reputation for honesty. If you consistently demonstrate it in small ways, people will trust you more, and at some point you will be able to make a larger "withdrawal" by doing something that requires their trust. If you are able to repay that large withdrawal, then you are rewarded with the ability to make even larger withdrawals. 

I think this is a useful view, but there is another view that offers different insights, and I haven't heard it clearly articulated before - at least not in a way that I understood.

## Reputation as a Compression Algorithm

This alternative model of reputation is that it helps us to efficiently store information about someone. Instead of remembering each time that a person did something nice, or supported us in sadness, or helped others at expense to themselves, we collapse this into remembering that someone is "kind". Then we use this lens to interpret their future actions. Note the difference from the "banking model" - the individual is not "drawing on" their reputation, so that they can do something that looks unkind. Instead, in the "compression model", we interpret the act as kind, based on our previous interactions.

This model predicts that reputation will be discontinous, because once a reputation is established, confirmation bias will help keep it in place. It also predicts that we will have more detailed reputations for people close to us. For those far away, we might only store one piece of information - like "good" or "bad"; "with me" or "against me"; or perhaps "trusted", "strong", "impartial", "helpful". We can spare more space for people closer to us.

Under this model, words are powerful. Creating a new category that unites many observations allows us to use it - without the word, the observations will be discarded, and will have basically no effect. Also, definitions of words are important - once someone is classified as "kind", our understanding of that word will change what we expect from the person. And changing a definition that we hold will change our views of others, even if their actions don't actually meet the definition - the compression has already occurred, so that information is lost.

This model seems related to ideas like "PR" or "spin", which look like attempts to manipulate our reputation-formation in various directions. And it explains the stickyness and power of nicknames.

I don't know what the practical implications of this model are - I do notice that I sometimes try to make my actions "predictable" or "legible" to others, so that I can benefit from a certain reputation that will make my life easier. For example, I want a reputation as reliable, so that my commitments have meaning. So I try to clearly name and delineate my commitments, so that others can model my behavior. I also hope that people will NOT expect me to do things that I haven't committed to, without decreasing my reputation for reliability.

