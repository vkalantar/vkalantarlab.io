---
title: Increasingly powerful corporations?
date: 2019-10-08
---

>At the level of the market, corporate governance is viewed with growing suspicion and distrust as **increasingly powerful corporations** pursue narrow self-interests at the expense of broader social and ecological concerns.
>
> — ISGP Reflections on Governance

![Comparison of size of East India company to modern companies](/img/company-comparisons-1.jpeg)

![List of largest companies ever](/img/company-comparisons-2.jpeg)

Is there a contradiction here?

***

Surprise, the answer is No.


Let's take a look at the South Sea company first. If the name sounds familiar, you might have heard of the South Sea bubble, which was an early example of a financial bubble in the company’s value which popped in… 1720.

Ironically, the South Sea Company supported the Bubble Act of 1920, which prohibited forming joint-stock companies without a royal charter… right before its value collapsed.

And if we take a look at the Mississippi Company, a holder of land in French colonies in North America, we see that its valuation also is dated to …1720. Its value also collapsed in the same financial bubble that affected the South Sea bubble.

Now let’s take a look at the elephant in the room — the Dutch East India Company, with a valuation of $7.9 trillion, more than the FAANG companies + various others combined, AND its valuation is dated to the 1600’s. This is not a bubble. The Dutch East India Company (which is abbreviated as VOC for Dutch reasons) was granted a monopoly on the Dutch spice trade in 1602, and spent two centuries trading, colonizing the world, and fighting wars — employing tens of thousands of people at its peak, and making its investors rich. It’s hard to imagine Walmart fighting a war or colonizing an island, so surely there was some decline in corporate power?

Unfortunately I don’t have a great answer here, except to say “probably not”. My vague understanding is that VOC was heavily controlled by the Dutch government, in a way that Walmart isn’t, to the extent that it makes more sense to consider it as partly a branch of government.

Furthermore, Walmart may not have been fighting wars, but United Fruit came very close (and Chiquita has pleaded GUILTY to aiding terrorist organizations). Are these companies also essentially wings of the US government? I don’t have a good reason, but my sense is no. And Google’s decisions have international repercussions (see China + censorship). To what extent are these choices dictated by the US government, and how much power does Google have to cause its own international incidents? Maybe it’s best not to know.

So: despite surface appearances, joint-stock companies were not as large as today’s multinationals, which lends some evidence to the idea that corporate power is increasing.

And let’s end with a caveat:

![Historical GDP of various countries](/img/company-comparisons-3.jpg)

Maybe this article should have been about China and India instead.