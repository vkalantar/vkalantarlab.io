---
title: NEOM
date: 2019-11-22
---

>NEOM is a bold and audacious dream. It is a vision of what a New Future might look like (in fact, NEOM means, “new future”). It’s an attempt to do something that’s never been done before and it comes at a time when the world needs fresh thinking and new solutions.
>
>NEOM is the vision of His Royal Highness Crown Prince Mohammed Bin Salman and is a centerpiece of Saudi Arabia’s 2030 Vision plan to grow and diversify the Saudi economy and position the country to play a leading role in global development.
>
>NEOM will introduce a new model for urban sustainability and be a place that is focused on setting new standards for community health, environmental protection and the effective and productive use of technology.

![The future city of NEOM](/img/neom.jpg)

If you think about it enough, eventually a question will come to your mind: “What makes cities special? Why are people willing to pay outrageous amounts of money for tiny apartments? Couldn’t I just go out into the empty desert and build for way cheaper?”
And if you’re the Royal Highness Crown Prince Mohammed Bin Salman, then you don’t have to wonder. You can just do it.

***

The plans, first announced in 2017, are ambitious —investing over half a trillion dollars to build a city to push the boundaries in 16 sectors including energy, education, water, and biotech and to “ [offer] its inhabitants an idyllic lifestyle paired with excellent economic opportunities” — and to build it by 2030, as NEOM’s website promises.

However, leaked documents show that the real plans are even more ambitious. The crown prince wants to employ cloud seeding to improve the weather, robot maids, robot cage fights, and a robot dinosaur amusement park. Prince Fahd bin Sultan says “ I don’t want any roads or pavements. We are going to have flying cars in 2030”. And finally, there have been proposals for a giant… artificial… MOON.

![Death Star](/img/death-star.jpg)

Yes, a giant artificial moon.

***

At this point, you’re probably thinking this is just another hare-brained scheme by a ruler with too much money and power for anyone to say no to, with no actual plans, which is doomed to failure. But you would be wrong.

NEOM’s website also contained, until November 2019, an Excel spreadsheet listing over 3000 items that the new city will need, from screws, nuts, and bolts to hemorrheologic agents and posterior pituitary hormones.

Nowhere does this masterwork of central planning shine brighter than its list of fruits and vegetables, which includes:

>Canned or jarred apples  
>Canned or jarred apricots  
>Canned or jarred bananas  
>Canned or jarred barberries  
>Canned or jarred bearberries  
>Canned or jarred blackberries  
>Canned or jarred bilberries  
>Canned or jarred blueberries  
>Canned or jarred breadfruit  
>Canned or jarred cherimoyas  
>Canned or jarred cherries  
>Canned or jarred citrons  
>Canned or jarred cranberries  
>Canned or jarred currants  
>Canned or jarred dates  
>Canned or jarred dragon fruit  
>Canned or jarred figs  
>Canned or jarred gooseberries  
>Canned or jarred grapefruit  
>Canned or jarred table grapes  
>Canned or jarred raisin grapes  
>Canned or jarred wine grapes  
>Canned or jarred guavas  
>Canned or jarred huckleberries  
>Canned or jarred kiwi fruit  
>Canned or jarred kumquats  
>Canned or jarred lemons  
>Canned or jarred limes  
>Canned or jarred loquats  
>Canned or jarred mandarin oranges or tangerines  
>Canned or jarred mangoes  
>Canned or jarred melons  
>Canned or jarred mulberries  
>Canned or jarred myrtle  
>Canned or jarred nectarines  
>Canned or jarred oranges  
>Canned or jarred papayas  
>Canned or jarred passion fruit  
>Canned or jarred peaches  
>Canned or jarred pears  
>Canned or jarred persimmons  
>Canned or jarred pineapples  
>Canned or jarred plucots  
>Canned or jarred plums  
>Canned or jarred pomegranates  
>Canned or jarred pomelos  
>Canned or jarred quinces  
>Canned or jarred raspberries  
>Canned or jarred rhubarb  
>Canned or jarred rose hips  
>Canned or jarred sapotes  
>Canned or jarred saskatoon berries  
>Canned or jarred strawberries  
>Canned or jarred sugar apple  
>Canned or jarred tamarillo  
>Canned or jarred nominant fruits  
>Canned or jarred chokeberries  
>Canned or jarred olives  

And this is only the canned FRUITS. There is a whole list of canned vegetables, as well as a separate list of fresh fruits and fresh vegetables.

And we can’t forget, of course, the ORGANIC fresh fruits, organic fresh vegetables, organic canned fruits, and organic canned vegetables — each of which is listed individually. Strangely,the category for pureed fruit has only four entries, and there are no pureed vegetables. Toddlers accustomed to pureed carrots — stay away from NEOM.

If the entire list was this detailed, I might start to think that the project had a chance of success. Unfortunately, fruits and vegetables must have been a specialty of whichever poor intern/prince was in charge of making this spreadsheet — the category for “political systems and institutions” contains only three entries:
> Political bodies  
> Political officials  
> Legislative bodies and practices

I’ll bring the organic canned or jarred mulberries if you’ll bring the legislative bodies and practice.

Other notable items include:

>Seafood: Fish, Shellfish, Aquatic invertebrates

They should have found someone who loves fish as much as this person loves fruits and vegetables.

>Water resources development and oversight: Development, Oversight

But what about the water and the resources??

>Atomic and nuclear energy machinery and equipment: Nuclear reactor equipment

Clearly, nuclear energy is much simpler than oil. There are 6 items under “nuclear energy”, and 90 under “oil”. And if anyone was thinking that this nuclear program was purely for energy purposes, don’t worry - item 2021 on the list:

>Military services and national defense: Nuclear warfare

No sign of a giant artificial moon, unfortunately.

***

If you’ve read this far, and you make other bad decisions, you might be looking for some way to get involved in this project. Have no fear, just head on over to [the jobs page](https://careers.neom.com/jobs). You could be a park ranger, an executive assistant, or if you’re truly ambitious, a Senior Masterplanner.

Also, given that even Mohammed Bin Salman thinks that after the murder of Jamal Khashoggi, “no one will invest [in the project] for years”, we’ve set up a [GoFundMe](https://www.gofundme.com/f/help-the-saudis-build-neom).

If you want to bring a needed item to the city, here’s the [spreadsheet](https://drive.google.com/file/d/1D1B8LCs5RHmTSNHJgCgP_qdTbOKLlGxL/view?usp=sharing).