---
title: Personal and Political
date: 2020-04-10
---

It is a well-known failure mode to apply concepts from everyday life to questions of governance - from [treating the government budget like a household budget](https://www.theguardian.com/money/us-money-blog/2013/mar/26/federal-budget-household-finances-fed) to [expecting contrition from guilty countries, like we would from individuals](https://en.wikipedia.org/wiki/Treaty_of_Versailles).

Part of this is not thinking enough about the problem. But I think it can also arise from a desire for a consistent moral philosophy. And I think this cuts both ways - useful tools for governing collective life become harmful when applied to personal life.

**Freedom of speech**? Makes a lot of sense in the public sphere, less in a group of friends. 

**Harm reduction** (in general, not specifically with respect to drug use)? With someone you know well, you probably have a better chance of helping them make good decisions than the government would.

**The benefits of competition**? It's great for making prices lower, but it's not nearly as effective when applied on a small level.

**Providing equal opportunities**? A good ideal, and necessary for public life, but impossible to practice in personal life.

In your personal life, you are: 
- more interdependent with the people around you
- able to dedicate more time to understanding them 
- better suited to make a decision based on individual idiosyncracies, instead of general, scalable techniques
  
This all sounds really obvious, but I still think that, in general, I (and maybe others) tend to try to hard to reconcile concepts from collective life with my own everyday experiences. Overall, I think I'd be better off forbidding sharing of concepts between the personal and the political.