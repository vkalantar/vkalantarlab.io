---
title: Illegal streaming services
date: 2020-05-02
---

There are many websites (that I will not link to) where you can watch almost any movie or TV show absolutely free, and [completely legally](https://www.businessinsider.com/are-streaming-sites-legal-2014-4). But while these sites are legal to use, it is surprising if they were legal to run. So what's happening? How can these sites continue to exist? Do they make money? Why don't they get shut down? We will try to answer these questions by looking at the different players in the online streaming game. 

Despite the legality of the use of these free streaming sites, to distinguish them from Netflix I will call them illegal streaming services,

## The Viewers
The viewers have many options - they can use Netflix or other legal streaming services, they can use "traditional media" like cable TV or physical media, or even go full illegal and torrent. The benefits of illegal streaming over a torrent is the lower risk of being arrested or of contracting a virus, and major negative is increased complexity of use (compared to Netflix). For how many people is this a worthwhile tradeoff?

**Legal streaming:** 150 million people worldwide use Netflix. It's  #20 in the Alexa rankings, and the average user spends 3 hours a day using it. Its responsible for ~15% of internet traffic. 50 million use Disney+. An unspecified number use Amazon (since it's bundled with Amazon Prime, finding the number of subscribers is more difficult). 

**Torrents:** According to one website I found, but again will not link to, BitTorrent has 170 million users, and 40% of internet traffic takes place over BitTorrent protocols.If true, this suggests that the average BitTorrent user consumes more bandwidth than the average Netflix user - which intuitively makes sense. 

**Illegal streaming:** The streaming sites I know of were all low in the Alexa rankings - the highest was #2500. This seems unlikely to be outweighed by the greater number of these sites, compared to legal streaming which is dominated by a few major players. 

I conclude that streaming sites are relatively niche. And this seems to match my personal experience - someone who knows of, and is willing to use, illegal streaming sites, is also likely to torrent instead. I will make some very unprincipled assumptions - that only college students use streaming services, and that all college students do. In the US, that means 20 million people at any given time. I have no idea if that's an over-estimate or under-estimate.

## The advertisers
Illegal streaming sites make money through ads. I think. They don't exactly publish financial information. Let's see how much money they could potentially make.

If you're advertising on Facebook or Google, you can expect a cost-per-click of $1 and a click-through rate of 0.3%, for a per-ad cost of 0.3 cents. But the users of illegal streaming sites are a specific demographic, and the advertisers that are willing to give money to an illegal company are.... not exactly the cream of the crop. Let's assume half the click-through rate and cost-per click. Then each ad earns the streaming site about a tenth of a cent. Assuming 10 ads on a single page, that's 1 cent per TV show or movie. Let's broaden the distribution to 0.1c - 5c.

## The streaming sites
If the streaming sites make 1 cent per video viewed, how much does that view cost them? Netflix says that 1 hour of regular quality streaming is 1 GB. Amazon AWS will charge you 0.8 cents for streaming that gigabyte. There are also storage and ingestion charges, but they're one-time costs that I will ignore, since they're pretty low. This means that streaming companies are either money-losers, or we can discard lower estimates in our analysis above. So we're looking at a profit of ~0 to 4 cents per video. If there are 20 million viewers a day, that's up to 800k a day - 300 million dollars profit, and maybe 600 million in revenue. This is a "medium-sized" business, according to the internet. For comparison, in 2020, Netflix's revenue is ~15 billion and Disney's is ~60 billion. 

## Governments
Why don't governments usually take down these sites? They have - most famously in the 2012 takedown of Megaupload by the US Department of Justice. So why don't they usually do it - legal complications? Technical challenges? The difficulty of moving against an anonymous organization that can operate from anywhere in the world? Is it just not worth it?

# Remaining Questions
- Who actually builds these sites? Do you just email them with a resume?
- Where do the ads come from? They don't have a button for "buy an ad" like Google and Facebook do.
- What are the motivations of the people that run them? Are they driven by profit, philanthropy, philosophy?



