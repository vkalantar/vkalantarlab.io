---
title: A review of "Moral Mazes" by Robert Jackall
date: 2020-03-07
---

![Cover of Moral Mazes](/img/moral-mazes.jpg) 

*Moral Mazes* is an enjoyable book to read. It's full of amusing anecdotes that hint at fundamental truths, passages that give you a sense of "peering behind the veil" of world of corporate managers, and an overall tone that reassures us that we made the right choice not to strive for continual advancement - the the people who succeed aren't happy, or good. That we're better off as we are now. 

That's what makes it suspicious.

If this sounds intriguing, I'd recommend reading it just for fun. If you're not sure, [this selection of quotes](https://thezvi.wordpress.com/2019/05/30/quotes-from-moral-mazes/) captures many of the best moments. The rest of this review will not contain any review of the book - just a list of questions and no answers.

### Questions about corporate managers that I wish *Moral Mazes* had focused on
1. The author describes the world of corporate managers as completely disconnected from our everyday intuitions of morality. How do the individuals in that world perceive the gap between their personal morals and their "corporate morality"? How do they respond when they conflict? Is there a process of "radicalization" that leads them from morality to corporate morality?
2. The author states that forming and breaking alliances is the main work of middle managers. What causes alliances to form or break? What would make an individual a valuable ally - besides being generically "well-connected" or "talented"? How much of these alliances are based purely on self-interest towards advancing, and how much based on true shared goals, or personal likes and dislikes?
3. Related to the above - what are examples of the kinds of plans that middle managers make with these alliances? How do they use their allies? What are their end goals? What methods do the plans employ?
4. How much does making money (for the company) matter? The author seems to think that beyond a certain level it's not so important - can this really be true?
5. What are the rules of the corporate space, and how do they develop? How are defectors punished? 
6. What kinds of power do middle managers yield? What kinds of power do they desire? What kinds of power do they trade?
7. What motivates corporate managers? Money? Power? Desire for recognition. On a human level, what do they want, and how do they reconcile everything they want?

### Bonus - one question with an answer
There was one important question that I thought *Moral Mazes* did a good job answering. It describes a world of self-interest and disconnection from others, of constant management of appearances and disregard of substance, and of a lack of principles. What makes this the way it is? The book identifies several elements of the answer:
- **Anxiety:** Corporate managers constantly feel anxiety because their futures are decided by random decisions outside their control - the retirement of the CEO, the global stock market, or geopolitical events. They struggle to exert control in any way they can, but also recognize the futility of trying to succeed through making profit alone.
- **Lack of objective measures:** There is no way to evaluate the work of corporate managers, since success or failure is so contingent on external considerations and random chance. So bosses have more leeway to let their own biases sway them, and maintaining the appearance of success is more important than its substance.
- **Desire to feel comfortable:** Managers want to feel comfortable with the people they work with - they dont' want people who raise uncomfortable questions, or refuse to go along with the crowd. 

I crave more of this type of analysis that connects the behaviors of an individual to the structure of the community - like the ideal gas law for human society.
