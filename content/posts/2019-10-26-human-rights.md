---
title: Should you support human rights?
date: 2019-10-26
---

![A no-right-turn sign](/img/no-right-turn.jpeg)

***

**Objection**: Is human rights even a real thing?

**Response 1**: Human rights are an expression of universal moral principles, which in practice are codified through international agreements like the Universal Declaration of Human Rights.

**Response 2**: Human rights are a legal construct, and like any legal agreement between different countries, they are decided by the countries with the most power. This is a good thing, because richer and more powerful countries are more likely to have higher standards for human rights.

**Response 3**: Human rights are divinely ordained by God — we can recognize this and work to provide them to all of humanity, or ignore it at our peril.

***

**Objection**: For the sake of argument, let’s assume that the Universal Declaration of Rights is a valid list of human rights. Article 18 states that “everyone has the right to freedom of thought, conscience and religion”. This standard is clearly not being met in many parts of the world. Does this justify using force to safeguard human rights?

**Response 1 (Nationalist)**: No, because states have sovereignty over their citizens. Just because a state is violating its obligation towards its citizens does not allow other states to violate their obligation of respecting its sovereignty. Two wrongs don’t make a right.

**Response 2 (Realist)**: No, because states have sovereignty over their citizens. This system has prevented many conflicts, and if we weigh the harm done by the lack of human rights against the potential harm of the breakdown of international order, we must sacrifice human rights.

**Response 3 (Moralist)**: Yes, of course it does. Our care for others should not be restricted by the boundaries of countries. Countries that have recognized human rights should fight for the protection of those rights in other places.

**Response 4 (Cultural Relativist)**: No, we should not seek to enforce our own morality and cultural standards on others. From the outside, we make judgements from our own perspective, and completely misunderstand the situations and experiences of other people. Any intervention — military or otherwise — would only cause more problems, as it has time and again in the past.

***

**Objection**: What do we do if two human rights conflict in a certain scenario?

**Response 1**: This will never happen. If it does, one of the two must not apply, or you’re applying it wrong, or it must not be a real human right.

**Response 2**: If two human rights seem unable to be applied simultaneously, there is probably some powerful force operating that makes it seem impossible. For example, if letting “the will of the people [be] the basis of the authority of government” (Article 21) is leading to laws that restrict freedom of religion (Article 18), then public sentiment is probably being manipulated by demagogues in order to be elected. Deal with them, and the problem will be solved.

**Response 3**: Human rights are useful because they encourage prioritization. When they conflict, they are outside the realm of their usefulness. Stop asking this question — it damages the image of human rights as unassailable rules.

**Response 4**: If you can’t figure it out, back off and don’t interfere. It’s morally wrong to deny people their rights.

***

**Objection**: Do human rights change over time? If not, how do we know that we have the correct ones now?

**Response 1**: Yes, they change over time as the minimum standard increases. There are no “correct” human rights, and we should constantly be striving to raise the standard.

**Response 2**: No, they are fixed. If they were not enforced for the vast majority of human history, then that merely highlights the urgency of ensuring their enforcement now.

**Response 3**: No, they are fixed, and global trends headed towards universal acceptance and application. In a few lifetimes, the major issues plaguing humanity will be largely solved. The dark times are almost over.

**Objection**: What if- **Response 1,2,3,4**: Stop objecting and tell us what you really think.

**Answer**: Human rights, as per Universal Declaration, are a useful tool to set collective goals and motivate us. They are not, and should not, be our highest priority, and don’t have moral value in and of themselves — they must be justified in other terms. The concept can be harmful when it prevents incremental improvement in the name of upholding rights.

There are also true human rights, granted by God. To know God, to draw closer to Him, to acquire divine qualities. But no treaty is needed to establish them. No government can violate them. They don’t need to be protected. They just exist.

***

Sources:  
[UN Declaration of Human Rights](https://www.un.org/en/universal-declaration-human-rights/index.html)  
[A short history of human rights](http://hrlibrary.umn.edu/edumat/hreduseries/hereandnow/Part-1/short-history.htm)  
[Stanford Encyclopedia of Philosophy entry on human rights](https://plato.stanford.edu/entries/rights-human/)  
[Institute for Studies in Global Prosperity library](https://www.globalprosperity.org/library)