---
title: Why did Bloomberg win the American Samoan primary?
date: 2020-03-07
markup: "mmark"
---

After spending half a billion dollars on ads in just a few months, Michael Bloomberg has admitted defeat and dropped out of the 2020 Democratic primary. His only victory - to the [amusement](https://www.wsj.com/articles/mike-bloombergs-620-million-campaign-did-really-wellin-american-samoa-11583538043) [of](https://www.ft.com/content/e8c13296-5e6a-11ea-b0ab-339c2307bcd4) [many](https://www.washingtonpost.com/opinions/2020/03/04/bloombergs-problem-is-he-spent-too-little/) - was in American Samoa - a US territory with 4 primary delegates and no votes in the electoral college. As the saying goes - Michael Bloomberg has never lost an election... on an island. Perhaps he should run for president of North America.

In all seriousness, this win is actually a surprising achievement, especially given that one of his competitors Tulsi Gabbard was actually born in American Samoa (though she grew up in Hawaii). How did he do it? Let's consider some possibilities.

### 1. Random chance
Most states had tens or hundreds of thousands of primary voters. American Samoa had... 351. Turns out that not being able to vote for the president makes you very unlikely to want to vote in the primary. Nationally, Bloomberg was polling around 15% - maybe a lot of Bloomberg voters just happened to show up in American Samoa. Let's calculate the (approximate) odds that Bloomberg could win purely by chance.

First: what are the chances that he could win by a single vote: 176 to 175? This is a classic probability problem with a well known answer:

$$p(\textrm{176 votes}) = {351 \choose 176} (0.15)^{176} (0.85)^{175} \approx 10^{-54}$$

The probability for winning by any other number of votes is less than that of winning by one, so the total probability is not more than $$10^{-51}$$. It's interesting how a relatively small disadvantage can become absolutely insurmountable - even if people were $$40\%$$ likely to vote for Bloomberg, his odds of winning in American Samoa would only be $$10^{-16}$$. This can't be the right answer.

### 2. More money spent

This [article](https://www.hawaiinewsnow.com/2020/03/05/heres-why-bloomberg-scored-decisive-super-tuesday-win-american-samoa-over-gabbard/) is the only one I could find that tried to answer why Bloomberg won American Samoa, instead of just making fun of him. It suggests a few reasons why he won - but I'm not sure I found them convincing. Let's go one by one.

First, the obvious: Bloomberg spent lots of money. Looking at 538's fascinating [campaign ad spend visualizer](https://projects.fivethirtyeight.com/2020-campaign-ads/) gives a clear idea of just how much Bloomberg outspent all the other candidates. It also breaks ad spend by state, and how much money was spent on each specific ad. Unfortunately, for all its wealth of detail, it does not contain American Samoa. To get an estimate, we will look at the state also with a primary on Super Tuesday with the closest number of votes to American Samoa. This is Vermont with 16, but we will ignore it since it's Bernie's home state and likely a special case. That gives us Utah with 29, where Bloomberg spent $2 million, and everyone else spent $250k or less. So Bloomberg did spend lots of money on small states, and probably spent lots on American Samoa.

But this explanation just leaves more questions. If ad spending actually works like this, then why didn't Bloomberg win all the states?
 
Perhaps American Samoa has less immunity to political ads because it's ignored during the general election, and usually also ignored during the primary? I am not sure.

### 3. More campaign staff
Bloomberg had 7 campaign staff on American Samoa for weeks before the election, while none of the other candidates really had any. This partially addresses the problem with the last explanation - Bloomberg [did have more staff](https://ballotpedia.org/Presidential_election_key_staffers,_2020) than the other candidates, but not to the same level as he did in American Samoa.

### 4. Endorsement from Fa’alagiga Nina Tua’au-Glaude
This is the explanation that seems most reasonable to me. One person - an influential chief - supports Bloomberg, and then encourages people to vote for him. A couple hundred show up. If there is only one chief who cares about the Democratic primaries (not a bad assumption), then naively chance of this happening is 17%. This win is not replicable in other states because there are hundreds or thousands of people with the same level of influence as the chief, and in these cases Bloomberg's chances drop significantly, as shown earlier.

In conclusion - if you want your fifteen minutes of fame, skip the advertising and campaigning, and just convince Fa’alagiga Nina Tua’au-Glaude to endorse you. 

