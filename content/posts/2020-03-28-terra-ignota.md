---
title: Terra Ignota and the denial of human nature
date: 2020-03-28
---
(MAJOR spoilers for *Too Like the Lightning* and *Seven Surrenders*, the first two books of the *Terra Ignota series. This post assumes you have read the books - if you haven't, I highly encourage you NOT to read the post. I have only read those two, so maybe some of these ideas won't make sense.)
<img src="/img/too-like-the-lightning.jpg" width="200">   
<img src="/img/seven-surrenders.jpg" width="200"> 

OK, after that break I assume you've read the books.

*Terra Ignota* is a hard science fiction story, except it's about sociology and not physics. It explores potential future institutions and ways of life, in a future of high-speed global travel and other technological advances. There are many interesting things you could say about the world it depicts, but I just want to draw out a single theme - the consequences of denying human nature.

### Denying Human Nature in Terra Ignota
There are four clear examples where the future world society of Terra Ignota seeks to deny human nature - and specifically in ways that are natural extensions of our current real-world tendencies.. A final warning: ALL of these examples are spoilers.

1. **Gender** - The society presented to us is one in that has achieved total gender equality - to the extent that using gendered pronouns, or commenting on or even noticing someone's gender is absolutely taboo. The downside of this utopia is that it has left humanity defenseless to seduction by those who conform to ancient gender norms - a bit farfetched if you ask me. But what seems far less farfetched is the reveal at the end of *Seven Surrenders* that, despite the attempted eradication of the notion of gender, the Cousins had founded a Hive that appealed to the female social role - care for children, compassion for the poor and needy, taking care of others.

2. **War and violence** - Again, the society presented to us is a utopia where war and personal violence have been eradicated - unless the individual chooses to experience it. And again, the end of the second book ends in the breakout of war, with suggestions that war would have happened eventually, even if the heroes of the story had prevented it this time.

3. **Collective Religion** - The future's solution to religious conflict is an extension of the present-day idea of religion as a personal preference, and secularism as a collective agreement. In the future, no one (except for your sensayer) is allowed to know your religious beliefs, to prevent the formation of collective religious groups, which would lead to religious conflict. Again, this structure breaks down in the final pages of the second book, as a group emerges that worships J.E.D.D Mason as a god.

4. **????** - I can't remember, but I previously thought of a fourth

Are these really forces of human nature that are too strong to be broken by the will of society? Is that just the tendency to assume that our own circumstances are reality? Or does Ada Palmer just really like the aesthetic of the Enlightment Era?

It doesn't matter - there's a more interesting question to consider...

### Denying Human Nature in our own society

1. After attending my first online wedding today (the in-person wedding was cancelled because the government has banned large gatherings as a response to the Covid-19 pandemic) (I can only hope this blog survives long enough that this context becomes necessary), I realized something new about Terra Ignota - they never hold video conferences. True - flying cars exist, but they still can take several hours to travel across the world. All important conversations - and, for the most part, all conversations - take place in person. Perhaps this is just a narrative device - despite the best efforts of YA authors of the early 2000's, nobody wants to read chat room threads or text messages in a book.  But a part of me insists it's unnatural for us to have conversations with people who aren't physically present - that something essential is being missed. And enthusiasm for working from home makes me think that we might be building structures that can't stand up to the pressure of our need for in-person contact.

2. Another of *Terra Ignota*'s fascinating social institutions is the 'bash - a self-selected group of 15-20 people who live together and raise their children together. It's the future's solution to a problem we are experiencing now - the complete disintegration of th extended family. Maybe this is one of those elements of our nature that we are denying now, and that the problems arising from this denial will eventually force change. I could plausibly see something like the 'bash system developing - although I am sure other futures are possible as well.

Are there other aspects of our nature that our society denies? Have past societies also been broken by their attempts to suppress an insuppressible aspect of humanity?



