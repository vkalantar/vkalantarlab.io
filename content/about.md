---
title: About
date:  2019-12-29
---

This is a blog about economics, governance, religion, and other topics, based on conversations with my brothers. I try to update every week.